# MySalary

## Описание:

«MySalary» предоставляет пользователям возможность узнавать размер текущей зарплаты, а также дату следующего повышения.

## Стек технологий:
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

![FastAPI](https://img.shields.io/badge/FastAPI-005571?style=for-the-badge&logo=fastapi)

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)

## Как запустить проект:

- Склонируйте репозитрий на свой компьютер и перейти в него в командной строке.

- Cоздайте и активировируйте виртуальное окружение:

```
python3 -m venv venv
```

* Если у вас Linux/macOS

    ```
    . venv/bin/activate
    ```

* Если у вас windows

    ```
    . venv/scripts/activate
    ```
- Установите poetry:

```
pip install poetry
```

- Установите зависимости коммандой:

```
poetry install
```

- Создайте .env файл в корневой директории, в котором должны содержаться следующие переменные:

```
DATABASE_URL=your_database
SECRET=your_secret
FIRST_SUPERUSER_EMAIL=your_email
FIRST_SUPERUSER_PASSWORD=your_password
```

- Примените миграции коммандой:

```
alembic upgrade head
```

- Запустить проект можно коммандой:

```
uvicorn app.main:app
```

- Весь функционал будет доступен по адреу `http://127.0.0.1:8000`

## Документация к проекту

Документация для API после установки доступна по адресу `http://127.0.0.1:8000/docs`

## Создание и запуск Docker-контейнера

- Из корневой директории проекта выполните комманду:

```
docker build -t shift .
```

- После завершения создания образа запустите контейнер коммандой:

```
docker run -d --name shift -p 8000:8000 shift 
```

- Весь функционал проекта будет доступен по адресу `http://localhost:8000`
- Документация проекта будет доступна по адресу `http://localhost:8000/docs`


## Автор

[Евгений Малый](https://github.com/SidVi990)

