FROM python:3.11-slim

RUN mkdir /alembic
RUN mkdir /app

RUN pip install poetry
COPY poetry.lock pyproject.toml /
RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

COPY app/ /app
COPY alembic/ /alembic
COPY alembic.ini /
COPY .env /

RUN alembic upgrade head

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]