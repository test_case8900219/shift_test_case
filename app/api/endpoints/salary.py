from typing import List

from fastapi import APIRouter, Depends, status
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.validators import check_salary_exists, check_user_id_duplicate
from app.core.db import get_async_session
from app.core.user import current_superuser, current_user
from app.crud.salary import salary_crud
from app.models import User
from app.schemas.salary import SalaryCreate, SalaryDB, SalaryUpdate

router = APIRouter()


@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    response_model=SalaryDB,
    response_model_exclude_none=True,
    dependencies=[Depends(current_superuser)],
)
async def create_salary(
    salary: SalaryCreate,
    session: AsyncSession = Depends(get_async_session),
):
    """Только для суперюзеров"""
    await check_user_id_duplicate(salary.user_id, session)
    new_salary = await salary_crud.create(salary, session)
    return new_salary


@router.get(
    "/all",
    response_model=List[SalaryDB],
    response_model_exclude_none=True,
    dependencies=[Depends(current_superuser)],
)
async def get_all_salary(
    session: AsyncSession = Depends(get_async_session),
):
    """Только для суперюзеров"""
    all_salary = await salary_crud.get_multi(session)
    return all_salary


@router.get(
    "/",
    response_model=SalaryDB,
    response_model_exclude_none=True,
)
async def get_salary(
    session: AsyncSession = Depends(get_async_session),
    user: User = Depends(current_user),
):
    salary = await salary_crud.get(user.id, session)
    return salary


@router.patch(
    "/{salary_id}",
    response_model=SalaryDB,
    dependencies=[Depends(current_superuser)],
)
async def update_salary(
    salary_id: int,
    obj_in: SalaryUpdate,
    session: AsyncSession = Depends(get_async_session),
):
    """Только для суперюзеров"""
    salary = await check_salary_exists(salary_id, session)
    salary_update = await salary_crud.update(salary, obj_in, session)
    return salary_update


@router.delete(
    "/{salary_id}",
    response_model=SalaryDB,
    dependencies=[Depends(current_superuser)],
)
async def delete_salary(
    user_id: int,
    session: AsyncSession = Depends(get_async_session),
):
    """Только для суперюзеров"""
    salary = await check_salary_exists(user_id, session)
    return await salary_crud.remove(salary, session)
