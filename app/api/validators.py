from fastapi import HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.salary import salary_crud


async def check_salary_exists(
    salary_id: int,
    session: AsyncSession,
):
    salary = await salary_crud.get_salary_by_id(salary_id, session)
    if salary is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Информация о зарплате не найдена!",
        )
    return salary


async def check_user_id_duplicate(
    user_id: int,
    session: AsyncSession,
):
    salary_id = await salary_crud.get_salary_id_by_user_id(user_id, session)
    if salary_id is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Информация о зарплате для этого пользователя уже существует!",
        )
