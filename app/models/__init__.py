__all__ = (
    "Salary",
    "User",
)

from .salary import Salary
from .user import User
