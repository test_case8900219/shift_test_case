from datetime import date

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from app.core.db import Base


class Salary(Base):
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"), unique=True)
    current_salary: Mapped[int] = mapped_column(default=0)
    next_increase: Mapped[date] = mapped_column(default=date.today())
