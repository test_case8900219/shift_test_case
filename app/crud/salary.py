from typing import Optional

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.base import CRUDBase
from app.models.salary import Salary


class CRUDSalary(CRUDBase):

    async def get(
        self,
        user_id: int,
        session: AsyncSession,
    ):
        db_obj = await session.execute(
            select(self.model).where(self.model.user_id == user_id)
        )
        return db_obj.scalars().first()

    async def get_salary_id_by_user_id(
        self,
        user_id: int,
        session: AsyncSession,
    ) -> Optional[int]:
        db_salary_id = await session.execute(
            select(Salary.id).where(Salary.user_id == user_id)
        )
        db_salary_id = db_salary_id.scalars().first()
        return db_salary_id


salary_crud = CRUDSalary(Salary)
