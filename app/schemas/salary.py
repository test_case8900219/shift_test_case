from datetime import date
from typing import Optional

from pydantic import BaseModel, Extra, PositiveInt


class SalaryBase(BaseModel):
    current_salary: Optional[PositiveInt]
    next_increase: Optional[date]

    class Config:
        extra = Extra.forbid


class SalaryCreate(BaseModel):
    user_id: int
    current_salary: PositiveInt
    next_increase: date


class SalaryUpdate(SalaryBase):
    pass


class SalaryDB(SalaryCreate):
    id: int
    user_id: int
    current_salary: PositiveInt
    next_increase: date

    class Config:
        from_attributes = True
