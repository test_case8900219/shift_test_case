"""Импорты класса Base и всех моделей для Alembic."""

from app.models import Salary, User  # noqa

from .db import Base  # noqa
