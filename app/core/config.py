from pathlib import Path
from typing import Optional

from pydantic import EmailStr
from pydantic_settings import BaseSettings

BASE_DIR = Path(__file__).parent.parent


class Settings(BaseSettings):
    app_title: str = "MySalary"
    description: str = "Сервис просмотра текущей зарплаты"
    database_url: str = f"sqlite+aiosqlite:///{BASE_DIR}/db.sqlite3"
    secret: str = "SECRET"
    first_superuser_email: Optional[EmailStr] = None
    first_superuser_password: Optional[str] = None

    class Config:
        env_file = ".env"


settings: Settings = Settings()
